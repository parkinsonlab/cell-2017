#include "Enzyme.h"
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

////////////////////////////////////////////////////////////////////
extern SBMLDocument_t *d;
extern Model_t *m;
extern Species_t **s;
extern map<int, Species_t *> e;
extern map<int, Species_t *> subs;
extern map<int, Species_t *> prot;
extern map<int, Species_t *> mets;
extern string classInfo;
extern int numSpecies; 
extern int numEnzyme;
extern int numSubstrate;
extern int numProtein;
extern int numMetabolite;
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;
//extern int *numMolEnzyme;
//extern int *numMolSubstrate;
extern int *numMolecule;
extern Reaction_t **rxns;
extern map<int, Species_t *> reactants;
extern map<int, Species_t *> products;
extern map<int, KineticLaw_t *> klaw;
extern map<int, Species_t *> initialMet;
extern map<int, Species_t *> finalMet;
extern map<int, Species_t *> finalSigEnzyme;
extern map<string, int> protType;
extern map<int, int> metIndex;
                                            
const static float REACTION_SPEED_FACTOR = 1.0e3;

Enzyme::Enzyme(moltype t) : IntracellularMolecule(){
    type = t;
    init();
    reactant = -1;
    product = -1;
}

Enzyme::Enzyme(moltype t, Point & loc) : IntracellularMolecule(loc){
    type = t;
    init();

}


//////////////////////////////////////////////////////////////////////
// Destructor for the Enzyme object
//
Enzyme::~Enzyme(){

    // Destructing the Enzyme
    printf("Deleting Enzyme\n");
}


//////////////////////////////////////////////////////////////////////
// Get group
//
int Enzyme::getGroup(){
    return MOL_GROUP_ENZYME;
}



//////////////////////////////////////////////////////////////////////
// Get group name
//
string Enzyme::getGroupName(){
    return MOL_GROUP_NAME_ENZYME;
}

moltype Enzyme::getType(){
    
        //int j = compareEnzymeName(type, &molEnzyme);
        //if(i == j)
        //    if(type != i)
        //        continue;
        //    else
        //        return j;
        //else
        //    if(type != j)
        //        continue;
        //    else
        //        return j;
        //cout << j << endl;
        return type;
}

string Enzyme::getTypeName(){

    // loop through number of enyzmes present
    // and compare the current enzyme's name to any previous enzymes' 
    // if they are same; if they are then assign enumeration for previous
    // enzyme's to the type
    /*for(int i=0; i<numEnzyme; i++)
    {
        int j = compareEnzymeName(i, &molEnzyme);
        //if(j == i)
        //    if(type != i)
        //        continue;
        //    else
        //        return Species_getName(s[i]);
        //else
        //    if(type != j)
        //        continue;
        //    else
        //        return Species_getName(s[j]);
        return Species_getName(s[j]);
    }*/
    for(map<string, int>::const_iterator protI = protType.begin(); protI != protType.end(); protI++) {
        if (type == protI->second) {
            return protI->first;
        }
    }
    return " ";
}

/*int Enzyme :: compareEnzymeName(int type, multimap<int, string>*molEnzyme)
{
    // Temporary iterators for storing enzyme types and names
    //multimap<int, string>::iterator temp1;
    //multimap<int, string>::iterator temp2;

    int j;

    //temp1 = molEnzyme->find(type);
    //string firstEnzymeName = (temp1->second);
    
    for(j=0; j<numEnzyme; j++)
    {
        //temp2 = molEnzyme->find(j);
        //if(strcmp(firstEnzymeName.c_str(), (temp2->second).c_str()) == 0)
        //    break;
        if(strcmp(Species_getName(e[type]), Species_getName(e[j])) == 0)
            break;
    }

    
    return j;
}*/

        // Checking if there are same enzymes
void Enzyme :: init(){
    // i stores the type of molecule
    /*int i=0;
    for(int j=0;j<numEnzyme;j++) {
        if(strcmp(Species_getId(e[type]), Species_getId(e[j])) == 0) {
            i = j;
               break;
           }
    }*/
    /*    // check if there are same enzymes in different comparments

    // two iterators for storing Enzyme names and corresponding enumeration

    //multimap<int, string>::iterator temp1;
      //multimap<int, string>::iterator temp2;

      // loop through number of enzymes and check if the current enzyme
      // is first occurrence of the type
      // if not, the assign enumeration of first occurence of the current
      // enzyme

    int m = compareEnzymeName(type, &molEnzyme);

    if(m == type)    // if there is no previous occurrence of enzyme
        i = type;    // else assign k to the type of current enzyme
    else
        i = m;    // else assign the first occurrence of enzyme to i*/
  
      mobility = 1.0;       // for now
      reactionRate = 0.2;   // for now

                 // enumeration for reactant type
    /*if(numEnzyme == i)
        product = i;      // if the reactant is the last substrate in the reaction, assign it as product
    else
        product = i+1;    // assign the following substrate as product*/
}

void Enzyme :: setReactant(int rct) {
    reactant = rct;
}

void Enzyme :: setProduct(int prd) {
    product = prd;
}

void Enzyme :: setParameters (float fwd, float rvrs, float equil, float max) {
    forwardReactionRate = fwd;
    reverseReactionRate = rvrs;
    equilibriumConstant = equil;
    maximumRate = max;
}

void Enzyme :: setParameters () {
    // assign random number to each of following reaction rates
    srand((unsigned)time(0));
    forwardReactionRate = rand() / 1000.0 / 1000.0;
    reverseReactionRate = rand() / 1000.0 / 1000.0;
    equilibriumConstant = rand() / 1000.0 / 1000.0;
    maximumRate = rand() / 1000.0 / 6.02e23 * REACTION_SPEED_FACTOR;
}