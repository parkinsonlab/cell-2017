#include <wx/wx.h>
#include <wx/statline.h>

#include "_wxClass.h"

class SimSidebar : public wxPanel {
    wxCheckBox      *cb_continueSimulation;
    wxSlider        *sl_timeInterval;
    wxCheckBox      *cb_showMetaConc, *cb_showMetaEnz, *cb_showEnv, *cb_showEnzyme;

    wxCheckBox      *cb_dispConc;

    wxRadioButton   **rb_showMetabolites;

    wxCheckBox      *cb_showCytosol, *cb_showEnv1, *cb_showEnv2, *cb_showEnv3, *cb_showEnv4;

    wxCheckBox      *cb_rotate, *cb_translate, *cb_scale;
    wxButton        *pb_close;

public:
    SimSidebar(wxFrame *parent);
    wxCheckBox      *cb_displayStats;

    // events
    void            ContinueSimulation(wxCommandEvent& event);
    void            SimulationSpeed(wxCommandEvent& event);
    void            ShowMetaConc(wxCommandEvent& event);
    void            ShowMetaEnz(wxCommandEvent& event);
    void            ShowEnv(wxCommandEvent& event);
    void            ShowConcType(wxCommandEvent& event);
    void            Rotate(wxCommandEvent& event);
    void            Translate(wxCommandEvent& event);
    void            Scale(wxCommandEvent& event);
    void            DisplayStats(wxCommandEvent& event);
    void            Close(wxCommandEvent& event);
};
