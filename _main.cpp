#include "_main.h"

extern ParameterManager *pm;

extern map<int, Species_t *> prot;    // Proteins
extern map<int, Species_t *> mets;    // Metabolites
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;

SimCanvas *simCanvas;
SimSidebar *simSidebar;

wxFrame *menuFrame, *simFrame;
wxFrame *aboutFrame = NULL;

IMPLEMENT_APP(CellApp)

bool CellApp::OnInit() {
    #ifndef MAC_OS
        int i = wxApp::argc;
        glutInit(&i, wxApp::argv);
    #endif
    if (wxApp::argc == 3) {
        if (strcmp(wxApp::argv[1], "--no-graphics") == 0 && strstr(wxApp::argv[2], ".xml") != NULL) {
            pm->loadSBML(wxApp::argv[2]);
        } else if (strcmp(wxApp::argv[2], "--no-graphics") == 0 && strstr(wxApp::argv[1], ".xml") != NULL) {
            pm->loadSBML(wxApp::argv[1]);
        } else {
            cout << "\"" << "Wrong syntax!" << endl;
            exit(1);
        }

        for(map<int, Species_t*>::const_iterator enz = prot.begin(); enz!=prot.end(); enz++) {
            char *nameEnzyme = (char *) Species_getName(enz->second);
            molEnzyme.insert(pair<int, string> (enz->first, (string)nameEnzyme));
        }
        for(map<int, Species_t*>::const_iterator met = mets.begin(); met!=mets.end(); met++) {
            char *nameSubstrate = (char *) Species_getName(met->second);
            molSubstrate.insert(pair<int, string> (met->first, (string)nameSubstrate));
        }

        SimEngine *simulation = new SimEngine();
        simulation->continueSimulation = true;
        while (1) { simulation->simulate(); }
    }
    if (wxApp::argc == 2) {
        if (strstr(wxApp::argv[1], ".xml") != NULL) {
            cout << "Finished reading in " << wxApp::argv[1] << endl;
            ShowSimulation(wxApp::argv[1].c_str(), wxApp::argv[1]);
        } else {
            cout << "\"" << wxApp::argv[1] << "\" is not a valid SBML XML Document!" << endl;
            exit(1);
        }
    } else {
        ShowMenu();
    }
    return true;
}

void CellApp::ShowMenu() {
    menuFrame = new wxFrame((wxFrame *)NULL, wxID_ANY,  wxT("Cell++ Diffusion Simulation"), wxPoint(100, 100), MENU_SIZE);
        wxPanel *panel = new wxPanel(menuFrame, wxID_ANY);

            wxStaticText *heading = new wxStaticText(panel, -1, wxT("Cell++\nDiffusion\nSimulation"), wxPoint(100, 10), wxSize(200, 40), wxALIGN_CENTRE_HORIZONTAL);
            wxFont font = heading->GetFont();
            font.SetPointSize(20);
            font.SetWeight(wxFONTWEIGHT_BOLD);
            heading->SetFont(font);

            MACRO_INIT(-1);
            BUTTON(panel, "Simulation from File",   100, 100,    200, 25,    CellApp::SimulationFromFile);
            BUTTON(panel, "DEMO Cascade.xml",       100, 130,    200, 25,    CellApp::DemoCascade);
            BUTTON(panel, "DEMO Glycolysis.xml",    100, 160,    200, 25,    CellApp::DemoGlycolysis);
            BUTTON(panel, "About",                  100, 220,    200, 25,    CellApp::AboutButton);

    menuFrame->Show();
}
void CellApp::ShowAbout() {
    aboutFrame = new wxFrame((wxFrame *)NULL, -1,  wxT("Cell++ Diffusion Simulation"), wxPoint(500, 100), ABOUT_SIZE);

        wxHtmlWindow *html = new wxHtmlWindow(aboutFrame);
        html->LoadPage("about.html");

    aboutFrame->Show();
}
void CellApp::ShowSimulation(const char *SBMLFilePath, wxString title) {
    title = "Cell++ Diffusion Simulation - " + title;
    simFrame = new wxFrame((wxFrame *)NULL, -1,  title.c_str(), wxPoint(100, 100), SIM_SIZE);

        int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};
        simCanvas = new SimCanvas((wxFrame*) simFrame, args, SBMLFilePath);
        simSidebar = new SimSidebar((wxFrame*) simFrame);

        wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
            sizer->Add(simCanvas, 1, wxEXPAND);
            sizer->Add(simSidebar, 0, wxEXPAND);

        simFrame->SetSizer(sizer);
        simFrame->SetAutoLayout(true);

    simFrame->Show();
    menuFrame->Close();
    if (aboutFrame != NULL) {
        aboutFrame->Close();
    }
}


void CellApp::SimulationFromFile(wxCommandEvent& event){
    wxString filename = wxFileSelector(wxT("Choose SBML XML Document"), wxT(""), wxT(""), wxT(""), 
        wxT("SBML XML Document (*.xml)|*.xml"), wxFD_OPEN);
    if (filename == "") return ;
    SBMLDocument_t *SBMLDoc = readSBML(filename);
    if (SBMLDoc->getNumErrors() > 0) {
        wxMessageBox( "Error found in file \"" + filename + "\"!", 
                      "Cell++ Diffusion Simulation", wxOK | wxICON_ERROR );
        return ;
    }
    ShowSimulation(filename.c_str(), filename);
}
void CellApp::DemoCascade(wxCommandEvent& event){
    ShowSimulation("Cascade.xml", "DEMO Cascade.xml");
}
void CellApp::DemoGlycolysis(wxCommandEvent& event){
    ShowSimulation("Glycolysis.xml", "DEMO Glycolysis.xml");
}
void CellApp::AboutButton(wxCommandEvent& event){
    ShowAbout();
}
