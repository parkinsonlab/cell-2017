// include OpenGL
#ifdef MAC_OS
    #include <OpenGL/glu.h>
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#else
    #include <GL/glu.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif

#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/glcanvas.h>
#include <wx/wxhtml.h>

#include "_wxHeader.h"


class CellApp: public wxApp {
    virtual bool OnInit();
    void ShowMenu();
    void ShowAbout();
    void ShowSimulation(const char*SBMLFile, wxString title);

    // events
    void SimulationFromFile(wxCommandEvent& event);
    void DemoCascade(wxCommandEvent& event);
    void DemoGlycolysis(wxCommandEvent& event);
    void AboutButton(wxCommandEvent& event);
};