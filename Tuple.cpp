/*
===============================================================================

    FILE:  Tupple.cpp
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Tuple

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#include "Tuple.h"
#include <stdio.h>

Tuple :: Tuple(int num){
//printf("Tuple Created:\t%d\n",(long)this);
    numElements = num;

    tuple = new float [numElements];

    flush();
}

Tuple :: Tuple(Tuple * t){
    numElements = t->getNumElements();
    tuple = new float[numElements];
    for(int i = 0 ; i < numElements ; i ++){
        tuple[i] = t->tuple[i];
    }
}

Tuple :: ~Tuple(){
//printf("Tuple Deleted:\t%d\n",(long)this);
    delete [] tuple;
    //printf("Delete Tuple\n");
}

void Tuple :: add(Tuple * t){
    if(numElements == t->getNumElements()){
        for(int i = 0 ; i < numElements ; i++){
            tuple[i] += (t->tuple)[i];
        }
    }
}

float Tuple :: sum(){
    float value = 0;
    for(int i = 0 ; i < numElements ; i ++)
        value += tuple[i];
    return value;
}

void Tuple :: scalarize(float s){
    for(int i = 0 ; i < numElements ; i++){
        tuple[i] *= s;
    }
}

void Tuple :: multByElement(Tuple * t){
    if(numElements == t->getNumElements())
    for(int i = 0 ; i < numElements ; i ++)
        tuple[i] *= t->tuple[i];
}

void Tuple :: divByElement(Tuple * t){
        if(numElements == t->getNumElements())
        for(int i = 0 ; i < numElements ; i ++)
                tuple[i] = tuple[i]/(t->tuple[i]);
}


void Tuple :: copyValue(Tuple * t){
    if(numElements == t->getNumElements()){
        for(int i = 0 ; i < numElements ; i ++){
            tuple[i] = t->tuple[i];
        }
    }
}

void Tuple :: negate(){
    for(int i = 0 ; i < numElements ; i ++)
        tuple[i] = ((-1) * tuple[i]);
}

int Tuple :: getNumElements(){
    return numElements;
}

void Tuple :: display(){
    for(int i = 0 ; i < numElements ; i ++){
        printf("%f\t",tuple[i]);
    }
    printf("\n");
}

void Tuple :: flush(){
    for(int i = 0 ; i < numElements ; i ++){
        tuple[i] = 0;
    }
}
