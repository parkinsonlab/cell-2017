#include "GenSimulation.h"

using namespace std;

static GenSimulation * simulation = NULL;

extern SBMLDocument_t *d;
extern Model_t *m;
extern Species_t **s;
extern map<int, Species_t *> e;
extern map<int, Species_t *> subs;
extern map<int, Species_t *> prot;
extern map<int, Species_t *> mets;
extern string classInfo;
extern int numSpecies; 
extern int numEnzyme;
extern int numSubstrate;
extern int numProtein;
extern int numMetabolite;
extern multimap<int, string> molEnzyme;
extern multimap<int, string> molSubstrate;
//extern int *numMolEnzyme;
//extern int *numMolSubstrate;
extern int *numMolecule;
extern Reaction_t **rxns;
extern map<int, Species_t *> reactants;
extern map<int, Species_t *> products;
extern map<int, KineticLaw_t *> klaw;
extern map<int, Species_t *> initialMet;
extern map<int, Species_t *> finalMet;
extern map<int, Species_t *> finalSigEnzyme;
extern map<string, int> protType;
extern map<int, int> metIndex;
                                            
//////////////////////////////////////////////////////////////////////
// Get the instance of the Cell Simulation
// ->    Only allow one instance of the cell to exist at any one time
//         If not yet exist, create a new Cell Simulation
//         otherwise return the pointer to the current existing Cell Simulation
//
GenSimulation * GenSimulation::getSimulation(){
    // If the simulation cell Simulation not yet exist
    if(!isInstantiated){
        // Create new Cell Simulation
        simulation = new GenSimulation();
        // New Cell Simulation Created
        isInstantiated = true;
    }
    // Return the pointer to the current Cell Simulation
    return simulation;
}



//////////////////////////////////////////////////////////////////////
// Constructor for the GenSimulation
// ->    Create and initialize properties of the GenSimulation space
GenSimulation::GenSimulation():Simulation(){

    // Initialize Simulation
    initialDataInput();
}



//////////////////////////////////////////////////////////////////////
// Destructor for the GenSimulation
//
GenSimulation:: ~GenSimulation(){}



//////////////////////////////////////////////////////////////////////
//  Input of molecules to the GenSimulation Space
//  (number of molecules, molecule type, environment)
//
void GenSimulation::initialDataInput(){
/*
//    createMolecule(getI("NUM_MOL_ENZYME_A"), MOL_ENZYME_A, lEnv->firstEnv(MOL_ENZYME_A));
//    createMolecule(getI("NUM_MOL_ENZYME_B"), MOL_ENZYME_B, lEnv->firstEnv(MOL_ENZYME_B));
//    createMolecule(getI("NUM_MOL_ENZYME_C"), MOL_ENZYME_C, lEnv->firstEnv(MOL_ENZYME_C));
//    createMolecule(getI("NUM_MOL_ENZYME_D"), MOL_ENZYME_D, lEnv->firstEnv(MOL_ENZYME_D));
//    createMolecule(getI("NUM_MOL_ENZYME_E"), MOL_ENZYME_E, lEnv->firstEnv(MOL_ENZYME_E));
*/

    //multimap<int, string>::iterator temp1;
    //multimap<int, string>::iterator temp2;
    
    //temp1 = molEnzyme.find(0);
    /*for(int i = 0; i < numEnzyme; i++)
    {
        //temp1 = molEnzyme.find(i);
        int m;
        // check if there are same enzymes in different comparments
        // --> BUT!!! What if they are in more than two compartments?
        for(m = 0; m < i; m++)
        {
            //temp2 = molEnzyme.find(m);
            //if(strcmp((temp1->second).c_str(), (temp2->second).c_str()) == 0)
            //    break;
            if(strcmp(Species_getName(e[i]), Species_getName(e[m])) == 0)
                break;
        }

    //cout << "WHASSUP?! This is MC " << Species_getName(e[i]) << " here, singing " << i << " ?= " << m << endl;

        if(i > m)
            createMolecule(numMolEnzyme[i], m, lEnv->secondEnv(m));
        else
            createMolecule(numMolEnzyme[i], m, lEnv->firstEnv(m));
    }*/
    
    int i;
    for (map<int, Species_t*>::const_iterator pr = prot.begin(); pr != prot.end(); pr++) {
        bool isSecond = false;
        const char * curProtName = Species_getName(pr->second);
        i = protType[curProtName];
        for(map<int, Species_t*>::const_iterator potential = prot.begin(); potential->first < pr->first; potential++) {
            if (strcmp(Species_getName(potential->second), curProtName) == 0) {
                isSecond = true;
                break;
            }
        }
        if(isSecond) {
            //cout << "Second molecule is at #" << pr->first << " and is of type " << i << endl;
                createMolecule(numMolecule[pr->first], i, lEnv->secondEnv(i));
        } else {
            //cout << "First molecule is at #" << pr->first << " and is of type " << i << endl;
            createMolecule(numMolecule[pr->first], i, lEnv->firstEnv(i));
        }
    }    

#ifdef CALCIUM_ION_CHANNELS
    int maxX = (xDim + 1) / 10;
    int maxY = (yDim + 1) / 10;

    bool first = true;

    for (int x = 0; x < maxX; x++) {
        for (int y = 0; y < maxY; y++) {
            for (int i = 0; i < 2; i++) {
                int xIdx = RandomNG::randInt(x*10+1,x*10+9);
                int yIdx = RandomNG::randInt(y*10+1,y*10+9);

                Point * loc = new Point( xIdx, yIdx, 4 );

                MembranousMolecule * m = new MembranousMolecule;

                m->type = MOL_MEMBRANOUS_FIXED_PUMP;
                m->setPosition( *loc );
                m->setDiffusible( false );

                memMolTable[m->type].push_back(m);
                delete loc;

                xIdx = RandomNG::randInt(x*10+1,x*10+9);
                yIdx = RandomNG::randInt(y*10+1,y*10+9);

                loc = new Point( xIdx, yIdx, 4 );

                m = new MembranousMolecule;

                m->type = MOL_MEMBRANOUS_FIXED_GATE;

                if (first) {
                    m->state = GATE_IS_OPEN;
                    m->openedAt = 0;
                    first = false;
                }
                else {
                    m->state = GATE_IS_CLOSED;
                    m->openedAt = -1;
                }

                m->setPosition( *loc );
                m->setDiffusible( false );

                memMolTable[m->type].push_back(m);
                delete loc;
            }
        }
    }
#endif
    if(!finalSigEnzyme.empty()) {
        map<string, int>::const_iterator enzT = protType.find(Species_getName(finalSigEnzyme.begin()->second));
        finalSigType = enzT->second;
    }
}



//////////////////////////////////////////////////////////////////////
//  Create Molecule where following are specified
//  type: the type of molecule to create
//
Molecule * GenSimulation::createNewMolecule(int type){
    Enzyme * ez = new Enzyme(type);

    Species_t *tempR = NULL;
      Species_t *tempP = NULL;

    if(reactants.find(type)!=reactants.end()) {
        assert(tempR==NULL);
        tempR = reactants[type];
    }
    if(products.find(type)!=products.end()) {
        assert(tempP==NULL);
        tempP = products[type];
    }

    for (int sp=0; sp<numSpecies; sp++) {
        if(tempR!=NULL && strcmp(Species_getId(tempR), Species_getId(s[sp])) == 0) {
            ez->setReactant(sp);
        }
        if(tempP!=NULL && strcmp(Species_getId(tempP), Species_getId(s[sp])) == 0) {
            ez->setProduct(sp);
        }
    }

    if(klaw.find(type) != klaw.end()) {
        KineticLaw * kl = klaw[type];
        int numParam = KineticLaw_getNumParameters(kl);
        Parameter_t* tempParam;
        float fwd, rvs, eq, max;
        for(int j=0; j<numParam; j++) {
            tempParam = KineticLaw_getParameter(kl, j);
            const char* paramName = Parameter_getName(tempParam);
            if(strcmp(paramName, "Ka") == 0) {
                fwd = Parameter_getValue(tempParam);
            } else if(strcmp(paramName, "Kp") == 0) {
                rvs = Parameter_getValue(tempParam);
            } else if(strcmp(paramName, "Keq") == 0) {
                eq = Parameter_getValue(tempParam);
            } else if(strcmp(paramName, "Vm") == 0) {
                max = Parameter_getValue(tempParam);
            }
        }
        ez->setParameters(fwd, rvs, eq, max);
    } else {
        ez->setParameters();
    }

    return ez;
}



//////////////////////////////////////////////////////////////////////
//  Perform periodic modifications to the GenSimulation Space
//  ->    Temporal events updating behaviour/Properties of the GenSimulation
//
void GenSimulation :: updateSimulation(){


}



bool GenSimulation :: isSimulationCompleted(){

    return simulationCompleted;
}




//////////////////////////////////////////////////////////////////////
//  Display data collected from the Simulation
//
void GenSimulation :: displayResults(){
    map< string , int >::const_iterator itr = actEventTable.begin();

    for (itr = actEventTable.begin(); itr != actEventTable.end(); itr ++)
    {
        printf("~\t%s\t%d\n", (itr->first).c_str(), itr->second);
    }

    // Display total number of cycles to complete simulation
    printf("#\tTotal num of cycles\t%d\n",numCycles);
}
