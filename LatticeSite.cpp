/*
===============================================================================

    FILE:  LatticeSite.cpp
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Lattice Site

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#include "LatticeSite.h"

int LatticeSite::auto_id = 0;

//////////////////////////////////////////////////////////////////////
//  Constructor for the Lattice Environment
//
LatticeSite :: LatticeSite(Point & pt){

    id = auto_id;
    auto_id ++;

    // Default Environment
    environment = ENV_CYTOSOL;

    // Position of the Lattice Site
    pos.x = pt.x;    pos.y = pt.y;    pos.z = pt.z;
}



//////////////////////////////////////////////////////////////////////
//  Destructor for the Lattice Site
//
LatticeSite :: ~LatticeSite(){}



//////////////////////////////////////////////////////////////////////
//  Get the environment of the Lattice Site
//
int LatticeSite :: getEnvironment(){

    // return the environment of the Lattice Site
    return environment;
}



//////////////////////////////////////////////////////////////////////
//  Set the Lattice Site to have the specified environment
//
void LatticeSite :: setEnvironment(envtype envType){

    // Set the environment to 'envType'
    environment = envType;
}



//////////////////////////////////////////////////////////////////////
//  Display different concentrations at the Lattice Site
//
void LatticeSite :: display(){

}


int LatticeSite :: getID(){
    return id;
}

//////////////////////////////////////////////////////////////////////
//  Get the location of the Lattice Site
//
Point * LatticeSite :: getLocation(){
    return & pos;
}

//////////////////////////////////////////////////////////////////////
