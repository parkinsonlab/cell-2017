/*
===============================================================================

    FILE:  IntracellularMolecule.h
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Intracellular Molecule

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#ifndef IntracellularMolecule_H
#define IntracellularMolecule_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>
#include "Molecule.h"
#include "RandomNG.h"
#include "definition.h"
#include "functionLib.h"

class Vector3D;

class IntracellularMolecule: public Molecule{

    public:

        //////////////////////////////////////////////////////////////////////
        // Constructor and Destructor
        IntracellularMolecule();
        IntracellularMolecule(Point &);
        virtual ~IntracellularMolecule();

        //////////////////////////////////////////////////////////////////////
        // Operations on the IntracellularMolecule

        //////////////////////////////////////////////////////////////////////
        // Virtual Methods

        // Get the type name of the IntracellularMolecule
        virtual string getTypeName() = 0;

        // Get the type of the IntracellularMolecule
        virtual moltype getType() = 0;  // typedef int moltype

        // Get the group name of the molecule
        virtual string getGroupName();

        // Get the Group of the molecule
        virtual int getGroup();

        virtual void move();
        void move_recursive(float distance);
        int find_lattice_intersections(float intersections [], Point pos, Vector3D translation);
        int find_lattice_intersections_one_axis(
          float intersections [],
          float pos_one_axis,
          float translation_one_axis,
          float translation_magnitude,
          int num_intersections);

        static int compare_float (const void * va, const void * vb);

        //virtual void activate(Molecule *);

    protected:

        //////////////////////////////////////////////////////////////////////
        // Member variables

        // The Parameter Manager
        ParameterManager *pm;


}
;

#endif
