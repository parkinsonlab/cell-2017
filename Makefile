UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Darwin)
	OS_SPECIFIC_LIBS=-framework GLUT -framework OpenGL -DMAC_OS
else
	OS_SPECIFIC_LIBS=-lGL -lGLU -lglut
endif


PROG=simulation

OBJS=\
	SimEngine.o \
	Simulation.o \
	GenSimulation.o \
	Molecule.o \
	IntracellularMolecule.o \
	Enzyme.o \
	MembranousMolecule.o \
	RandomNG.o \
	ParameterManager.o \
	LatticeSite.o \
	LatticeEnvironment.o \
	Point.o \
	Vector3D.o \
	Tuple.o \
	Matrix.o

WXOBJS=\
	_SimCanvas.o \
	_SimSidebar.o \
	_StatsCanvas.o

COMPILE=g++
LIBS=-lm -lsbml
CFLAGS=-w -std=c++11

$(PROG): _main.cpp _main.h $(OBJS) $(WXOBJS)
	$(COMPILE) $(CFLAGS) -o $(PROG) $(OBJS) $(WXOBJS) _main.cpp $(LIBS) $(OS_SPECIFIC_LIBS) `wx-config --libs --cxxflags --gl-libs`

_SimCanvas.o: _SimCanvas.cpp _SimCanvas.h
	$(COMPILE) $(CFLAGS) -c -o $@ $< $(LIBS) $(OS_SPECIFIC_LIBS) `wx-config --libs --cxxflags --gl-libs`

_SimSidebar.o: _SimSidebar.cpp _SimSidebar.h
	$(COMPILE) $(CFLAGS) -c -o $@ $< $(LIBS) $(OS_SPECIFIC_LIBS) `wx-config --libs --cxxflags --gl-libs`

_StatsCanvas.o: _StatsCanvas.cpp _StatsCanvas.h
	$(COMPILE) $(CFLAGS) -c -o $@ $< $(LIBS) $(OS_SPECIFIC_LIBS) `wx-config --libs --cxxflags --gl-libs`

SimEngine.o: SimEngine.cpp SimEngine.h
	$(COMPILE) $(CFLAGS) -c -o $@ $< $(LIBS) $(OS_SPECIFIC_LIBS)

%.o: %.cpp %.h
	$(COMPILE) $(CFLAGS) -c -o $@ $< $(LIBS)

clean:
	rm -f *.o simulation
