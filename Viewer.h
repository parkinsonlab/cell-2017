/*
===============================================================================

    FILE:  Viewer.h
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Viewer

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

// Include OpenGL libraries
#include <GL/gl.h>
#include <GL/glut.h>
#include <X11/X.h>

#include <math.h>
#include <stdio.h>

#include "Point.h"
#include "Vector3D.h"

class Viewer{

    public:
        Viewer();
        void set(Vector3D eye, Vector3D look, Vector3D up);
        void roll(float angle);
        void pitch(float angle);
        void turn(float angle);
        void yaw(float angle);
        void rotate(float angle, int axis);
        void slide(float delU, float delV, float delN);
        void setShape(float vAng, float asp, float nearD, float farD);

    private:
        Vector3D eye;
        Vector3D u,v,n;
        double viewAngle, aspect, nearDist, farDist;
        void setModelViewMatrix();

};
