/*
===============================================================================

    FILE:  Vector3D.h
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Vector 3D

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#ifndef VECTOR3D_H
#define VECTOR3D_H

#include "Point.h"
#include <math.h>

class Vector3D{

    public:
        float x,y,z;

        Vector3D();
        Vector3D(float, float, float);
        Vector3D(const Vector3D &);

        Vector3D cross(const Vector3D &);
        float dot(Vector3D);

        void set(float,float,float);
        void set(const Vector3D &);
        void reverse();
        void display();
        void setDiff(Point &, Point &);
        float magnitude();
        void normalize();
        void scalarize(float);
};

#endif
