/*
===============================================================================

    FILE:  LatticeSite.h
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Lattice Site

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#ifndef LATTICESITE_H
#define LATTICESITE_H

#include <map>
#include <vector>
#include "definition.h"
#include "Point.h"

using namespace std;

class LatticeSite{

    public:

        // Constructor and Destructor for the Lattice Site
        LatticeSite(Point &);
        ~LatticeSite();


        // Get the location of the Lattice Site
        Point * getLocation();

        // Get the environemnt of the Lattice Site
        moltype getEnvironment();

        // Set the environment of the site to the specified type
        void setEnvironment(envtype);

        // Display properties of the Lattice Site
        void display();

        int getID();

        map <int, vector<LatticeSite *> > neighbors;

    private:

        static int auto_id;

        int id;

        // Positon/Location of the Lattice Site
        Point pos;

        // Environment of the Lattice Site
        envtype environment;

}
;

#endif
