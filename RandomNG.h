/*
===============================================================================

    FILE:  RandomNG.h
    
    PROJECT:

        Cell++ Cell Simulation Program

    CONTENTS:

        Random Number Generator

    PROGRAMMERS:

        Matthew LK Yip, Chris Sanford, Sukwon Oh

    COPYRIGHT:

        Copyright (C) <2005> <Mattew LK Yip, Chris Sanford, Sukwon Oh>

        This program is free software; you can redistribute it and/or modify it 
        under the terms of the GNU General Public License as published by the 
        Free Software Foundation; either version 2 of the License, or 
        (at your option) any later version.

        This program is distributed in the hope that it will be useful, but 
        WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
        or FITNESS FOR A PARTICULAR PURPOSE. 
        See the GNU General Public License for more details.

        You should have received a copy of the GNU General Public License along 
        with this program; if not, write to the 
        Free Software Foundation, Inc., 59 Temple Place, 
        Suite 330, Boston, MA 02111-1307 USA

    VERSION:

        Cell++ 0.93

    CHANGES:

        Original release (DA)

===============================================================================
*/

#ifndef RANDOMNG_H
#define RANDOMNG_H

#include <algorithm>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <vector>
#include <math.h>
#include <unistd.h>


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

using namespace std;

class RandomNG { 
    public:

        static int inext,inextp;
        static long ma[56];
        static int iff;

        static int randInt(int, int);
        static float randFloat(float, float);
        static float randNormal(float, float);
        static float randNegExp(float);
        static float randErlang(float, float);

    protected:

        RandomNG();

    private:

        static float orand();
        static long getSeed();

}; 

#endif
