#include "_wxHeader.h"

extern map<int, Species_t *> mets;

extern SimCanvas *simCanvas;
extern int concType;
StatsCanvas *statsCanvas = NULL;

wxFrame *statsFrame;

extern SimEngine *simulation;

SimSidebar::SimSidebar(wxFrame *parent)
       : wxPanel(parent, wxID_ANY, wxDefaultPosition, 
         wxSize(200, 540), wxBORDER_SUNKEN) {

    MACRO_INIT(10);

                            STATICTEXT( "Control", 10)

    cb_continueSimulation = CHECKBOX_E( this, "Run Simulation",            SimSidebar::ContinueSimulation);

                            STATICTEXT( "Simulation Speed", 20)
    sl_timeInterval = new   wxSlider(   this, obj_id, 160, 0, 160, wxPoint(20, y), wxSize(160, 20)); y+=20;
                                Connect(obj_id, wxEVT_SLIDER,              wxCommandEventHandler(SimSidebar::SimulationSpeed));

    cb_showMetaConc =       CHECKBOX_E( this, "Display Concentrations",    SimSidebar::ShowMetaConc);      cb_showMetaConc->SetValue(true);
        
    cb_showMetaEnz =        CHECKBOX_E( this, "Display Proteins",          SimSidebar::ShowMetaEnz);       cb_showMetaEnz->SetValue(true);
        
    cb_showEnv =            CHECKBOX_E( this, "Display Environment",       SimSidebar::ShowEnv);
    cb_showEnzyme =         CHECKBOX(   this, "Display Enzymes");

                            STATICLINE()


    int count = 1;
    for(map<int, Species_t*>::const_iterator mItr = mets.begin(); mItr!= mets.end(); mItr++) {
        if (mItr->first < 4) {
            count++;
        }
    }
    wxRadioButton * rb_showMetabolites [count];
    rb_showMetabolites[0] = new wxRadioButton(this, 99, "Show All Metabolites", wxPoint(10, y), wxDefaultSize, wxRB_GROUP);
    Connect(99, wxEVT_RADIOBUTTON, wxCommandEventHandler(SimSidebar::ShowConcType));
//    cb_showAll =            CHECKBOX(   this, "Show All Metabolites"); cb_showMetaEnz->SetValue(true);
    for(map<int, Species_t*>::const_iterator mItr = mets.begin(); mItr!= mets.end(); mItr++) {
        if (mItr->first < 4) {
            rb_showMetabolites[mItr->first + 1] = new wxRadioButton(this, mItr->first+100, Species_getName(mItr->second), wxPoint(20, y+20*(mItr->first+1)));
            Connect(mItr->first+100, wxEVT_RADIOBUTTON, wxCommandEventHandler(SimSidebar::ShowConcType));
        }
    }
    y += 100;

                            STATICLINE()

    cb_showCytosol =        CHECKBOX(   this, "Cytosol");
    cb_showEnv1 =           CHECKBOX(   this, "Membrane");
    cb_showEnv2 =           CHECKBOX(   this, "Env 2");
    cb_showEnv3 =           CHECKBOX(   this, "Env 3");
    cb_showEnv4 =           CHECKBOX(   this, "Env 4");

                            STATICLINE()

    cb_rotate =             CHECKBOX_E( this, "Rotate",                    SimSidebar::Rotate);            cb_rotate->SetValue(true);
    cb_translate =          CHECKBOX_E( this, "Translate",                 SimSidebar::Translate);         cb_translate->Disable();
    cb_scale =              CHECKBOX_E( this, "Scale",                     SimSidebar::Scale);             cb_scale->SetValue(true);
    cb_displayStats =       CHECKBOX_E( this, "Display Statistics",        SimSidebar::DisplayStats);

    pb_close =              BUTTON(     this, "Close", 10, y, 175, 25, SimSidebar::Close);
}

void SimSidebar::ContinueSimulation(wxCommandEvent& event){
    simulation->continueSimulation = cb_continueSimulation->IsChecked();
    simCanvas->Refresh();
}
void SimSidebar::SimulationSpeed(wxCommandEvent& event){
    simCanvas->simulation_timeout = (int) pow(2.0, (160-sl_timeInterval->GetValue())/16) - 1;
}
void SimSidebar::ShowMetaConc(wxCommandEvent& event){
    simulation->showMetaConc = cb_showMetaConc->IsChecked();
    simCanvas->Refresh();
}
void SimSidebar::ShowMetaEnz(wxCommandEvent& event){
    simulation->showMetaEnz = cb_showMetaEnz->IsChecked();
    simCanvas->Refresh();
}
void SimSidebar::ShowEnv(wxCommandEvent& event){
    simulation->showEnv = cb_showEnv->IsChecked();
    simCanvas->Refresh();
}
void SimSidebar::ShowConcType(wxCommandEvent& event) {
    concType = event.GetId()-100;
}
void SimSidebar::Rotate(wxCommandEvent& event){
    simCanvas->rotate_enabled = cb_rotate->IsChecked();
}
void SimSidebar::Translate(wxCommandEvent& event){}
void SimSidebar::Scale(wxCommandEvent& event){
    simCanvas->scale_enabled = cb_scale->IsChecked();
}
void SimSidebar::DisplayStats(wxCommandEvent& event){
    if (cb_displayStats->IsChecked()) {
        statsFrame = new wxFrame((wxFrame *)NULL, -1,  wxT("Statistics"), wxPoint(800, 100), STATS_SIZE);
        statsCanvas = new StatsCanvas(statsFrame, simCanvas->gl_args);
        statsFrame->Show();
    } else {
        statsFrame->Close();
        delete statsFrame;
    }
}
void SimSidebar::Close(wxCommandEvent& event){
    exit(0);
}